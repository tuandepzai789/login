import users from './source.js';
const getNameLogin = document.getElementById('nameLogin');
const getPassLogin = document.getElementById('passLogin');
document.getElementById('login-btn').addEventListener('click', () => {
   //handle input
   if(getNameLogin.value.length<1 && getPassLogin.value.length<1){
      return document.getElementById('warning-password').innerHTML="Bạn chưa nhập tên đăng nhập và mật khẩu";
   }
   if(getNameLogin.value.length>1 && getPassLogin.value.length<8){
      return document.getElementById('warning-password').innerHTML="Mật khẩu của bạn phải từ 8 ký tự trở lên.Vui lòng nhập lại mật khẩu!!"
   }
   //handle output
   const handleUser = (value,index) => {
      return value.name===getNameLogin.value;
   }
   let findElement = users.find(handleUser);
   if(!findElement){
      return document.getElementById('warning-password').innerHTML="Bạn đã nhập tên đăng nhập và mật khẩu không chính xác, vui lòng thử lại";
   }
   if(findElement.password!==getPassLogin.value){
      return document.getElementById('warning-password').innerHTML="Mật khẩu không chính xác.Vui lòng thử lại!!"
   }
   return window.location.href = "http://www.w3schools.com";
})
